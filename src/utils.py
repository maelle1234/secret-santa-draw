def input_names_tolist():
    l_names = []
    current_name = ''
    print('Please enter 0 if there are no more participants')
    while current_name != '0':
        current_name = input('Name of participant {}: '.format(len(l_names)+1))
        l_names.append(current_name)
    l_names.pop()
    return l_names


def generate_email_content(participant, name_drawn):
    return "Hello {}! This christmas, you should buy a gift for {}".format(participant, name_drawn)
