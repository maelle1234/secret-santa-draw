import random

def one_draw(names):
    '''
    Random draw for all names submitted.

    Parameters
    ----------
    names: list
        names of the participants
    
    Returns
    -------
    success: bool
        False if the random draw has paired any participant with themselves,
        True otherwise
    res: dict
        name drawn for each participant
    '''
    success = True
    remaining_names = [elt for elt in names]
    res = {}
    for n in names:
        # names_ok are all remaining names except the current one
        names_ok = [elt for elt in remaining_names if elt != n]
        try:
            draw = random.choice(names_ok)
        except IndexError:
            success = False
        else:
            res[n] = draw
            remaining_names.remove(draw)
    return success, res


def draw(names):
    '''
    Random draw for all names submitted that ensures 
    each participant is not paired with themselves.

    Parameters
    ----------
    names: list
        names of the participants
    
    Returns
    -------
    n_draws: int
        number of draws required to get this result
    res: dict
        name drawn for each participant
    '''
    success = False
    n_draws = 0
    while not success:
        success, res = one_draw(names)
        n_draws += 1
    return n_draws, res
