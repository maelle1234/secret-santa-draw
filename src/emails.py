# https://www.tutorialspoint.com/python/python_sending_email.htm


# FIRST let's use a local SMTP server
# python -m smtpd -c DebuggingServer -n localhost:1025

import smtplib

def send_email_toserver(from_addr, to_addr, content):
    header = ("From: %s\r\nTo: %s\r\n\r\n"
    % (from_addr, to_addr))
    msg = header + content

    server = smtplib.SMTP('localhost', 1025)
    server.set_debuglevel(1)
    server.sendmail(from_addr, to_addr, msg)
    server.quit()