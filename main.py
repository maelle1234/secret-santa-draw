from src.draw import draw
from src.emails import send_email_toserver
from src.utils import input_names_tolist, generate_email_content


if __name__ == '__main__':

    names = input_names_tolist()
    _, res = draw(names)
    print(res)

    for participant, name_drawn in res.items():
        email_content = generate_email_content(participant, name_drawn)
        send_email_toserver(participant, name_drawn, email_content)
        